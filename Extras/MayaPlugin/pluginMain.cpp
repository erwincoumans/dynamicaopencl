/*
Bullet Continuous Collision Detection and Physics Library Maya Plugin
Copyright (c) 2008 Walt Disney Studios
 
This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising
from the use of this software.
Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it freely,
subject to the following restrictions:
 
1. The origin of this software must not be misrepresented; you must
not claim that you wrote the original software. If you use this
software in a product, an acknowledgment in the product documentation
would be appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must
not be misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
 
Written by: Nicola Candussi <nicola@fluidinteractive.com>

Modified by Roman Ponomarev <rponom@gmail.com>
01/22/2010 : Constraints reworked
01/27/2010 : Replaced COLLADA export with Bullet binary export

Modified by Francisco Gochez <fjgochez@gmail.com>
Nov 2011 - Dec 2011 : Added logic for soft bodies
*/


#include "BulletSoftBody/btSoftBody.h"

//pluginMain.cpp
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <maya/MDGMessage.h>
#include <maya/MSceneMessage.h>

#include "mayaUtils.h"
#include "rigidBodyNode.h"
#include "rigidBodyArrayNode.h"
#include "collisionShapeNode.h"
#include "constraint/nailConstraintNode.h"
#include "constraint/hingeConstraintNode.h"
#include "constraint/sliderConstraintNode.h"
#include "constraint/sixdofConstraintNode.h"
#include "dSolverNode.h"
#include "dSolverCmd.h"
#include "dRigidBodyCmd.h"
#include "dRigidBodyArrayCmd.h" 
#include "constraint/dNailConstraintCmd.h" 
#include "constraint/dHingeConstraintCmd.h" 
#include "constraint/dSliderConstraintCmd.h" 
#include "constraint/dsixdofConstraintCmd.h" 
#include "mvl/util.h"
#include "bulletExport.h"
#include "colladaExport.h"
#include "softBodyNode.h"
#include "dSoftBodyCmd.h"

#include "Bullet3OpenCL/Initialize/b3OpenCLUtils.h"
#include "Bullet3Common/b3Logging.h"

cl_context g_contextCL=0;
cl_device_id g_deviceCL=0;
cl_command_queue g_queueCL=0;
bool g_isInitializedCL = false;
cl_platform_id g_platformCL=0;



void MayaPrintf(const char* msg)
{
	cout <<msg;
	flush(cout);
}
void MayaWarningMessage(const char* msg)
{
	cout <<msg;
	flush(cout);

}
void MayaErrorMessage(const char* msg)
{
	cout <<msg;
	flush(cout);

}



void initCL(int preferredDeviceIndex, int preferredPlatformIndex)
{
	void* glCtx=0;
	void* glDC = 0;
	int ciErrNum = 0;
	cl_device_type deviceType = CL_DEVICE_TYPE_GPU;
	g_contextCL = b3OpenCLUtils::createContextFromType(deviceType, &ciErrNum, 0,0,preferredDeviceIndex, preferredPlatformIndex,&g_platformCL);
	oclCHECKERROR(ciErrNum, CL_SUCCESS);
	int numDev = b3OpenCLUtils::getNumDevices(g_contextCL);
	if (numDev>0)
	{
		g_deviceCL= b3OpenCLUtils::getDevice(g_contextCL,0);
		g_queueCL = clCreateCommandQueue(g_contextCL, g_deviceCL, 0, &ciErrNum);
		oclCHECKERROR(ciErrNum, CL_SUCCESS);
		b3OpenCLUtils::printDeviceInfo(g_deviceCL);
		g_isInitializedCL = true;
	}
}

void exitCL()
{
	if (g_isInitializedCL)
	{
		clReleaseCommandQueue(g_queueCL);
		clReleaseContext(g_contextCL);
	}
}
const char *const bulletOptionScript = "bulletExportOptions";
const char *const bulletDefaultOptions =    "groups=1;"    "ptgroups=1;"    "materials=1;"    "smoothing=1;"    "normals=1;"    ;

const char *const colladaOptionScript = "bulletExportOptions";
const char *const colladaDefaultOptions =    "groups=1;"    "ptgroups=1;"    "materials=1;"    "smoothing=1;"    "normals=1;"    ;

///we need to register a scene exit callback, otherwise the btRigidBody destructor might assert in debug mode
///if some constraints are still attached to it
MCallbackId fMayaExitingCB=0;
void releaseCallback(void* clientData)
{
	solver_t::destroyWorld();
    solver_t::cleanup();

}



MStatus initializePlugin( MObject obj )
{

    MStatus   status;
	char version[1024];
	sprintf(version,"Bullet %d, build %s",BT_BULLET_VERSION,__DATE__);
#ifdef BT_DEBUG
	 sprintf(version,"Bullet %d, debug build %s",BT_BULLET_VERSION,__DATE__);
#else
	 sprintf(version,"Bullet %d, release build %s",BT_BULLET_VERSION,__DATE__);
#endif
	  MFnPlugin plugin( obj, "Walt Disney Feature Animation", version, "Any");

	  ///The developer can route b3Printf output using their own implementation
	b3SetCustomPrintfFunc(MayaPrintf);
	b3SetCustomWarningMessageFunc(MayaWarningMessage);
	b3SetCustomErrorMessageFunc(MayaErrorMessage);


	initCL(-1,-1);

    solver_t::initialize();


// Register the translator with the system
   status =  plugin.registerFileTranslator( "Bullet Physics export", "none",
                                          BulletTranslator::creator,
                                          (char *)bulletOptionScript,
                                          (char *)bulletDefaultOptions );

	MCHECKSTATUS(status,"registerFileTranslator Bullet Physics export");

#ifdef BT_USE_COLLADA
   status =  plugin.registerFileTranslator( "COLLADA Physics export", "none",
                                          ColladaTranslator::creator,
                                          (char *)colladaOptionScript,
                                          (char *)colladaDefaultOptions );

	MCHECKSTATUS(status,"registerFileTranslator COLLADA Physics export");

#endif
    
    //
    status = plugin.registerNode( rigidBodyNode::typeName, rigidBodyNode::typeId,
                                  rigidBodyNode::creator,
                                  rigidBodyNode::initialize,
                                  MPxNode::kLocatorNode );
    MCHECKSTATUS(status, "registering rigidBodyNode")
    MDGMessage::addNodeRemovedCallback(rigidBodyNode::nodeRemoved, rigidBodyNode::typeName);

    //
    status = plugin.registerNode( rigidBodyArrayNode::typeName, rigidBodyArrayNode::typeId,
                                  rigidBodyArrayNode::creator,
                                  rigidBodyArrayNode::initialize,
                                  MPxNode::kLocatorNode );
    MCHECKSTATUS(status, "registering rigidBodyArrayNode")
    MDGMessage::addNodeRemovedCallback(rigidBodyArrayNode::nodeRemoved, rigidBodyArrayNode::typeName);


    // 
    status = plugin.registerNode( collisionShapeNode::typeName, collisionShapeNode::typeId,
                                  collisionShapeNode::creator,
                                  collisionShapeNode::initialize,
                                  MPxNode::kDependNode );
    MCHECKSTATUS(status, "registering collisionShapeNode")

    //
    status = plugin.registerNode( nailConstraintNode::typeName, nailConstraintNode::typeId,
                                  nailConstraintNode::creator,
                                  nailConstraintNode::initialize,
                                  MPxNode::kLocatorNode );
    MCHECKSTATUS(status, "registering nailConstraintNode")
    MDGMessage::addNodeRemovedCallback(nailConstraintNode::nodeRemoved, nailConstraintNode::typeName);

    //
    status = plugin.registerNode( hingeConstraintNode::typeName, hingeConstraintNode::typeId,
                                  hingeConstraintNode::creator,
                                  hingeConstraintNode::initialize,
                                  MPxNode::kLocatorNode );
    MCHECKSTATUS(status, "registering hingeConstraintNode")
    MDGMessage::addNodeRemovedCallback(hingeConstraintNode::nodeRemoved, hingeConstraintNode::typeName);

	//
    status = plugin.registerNode( sliderConstraintNode::typeName, sliderConstraintNode::typeId,
                                  sliderConstraintNode::creator,
                                  sliderConstraintNode::initialize,
                                  MPxNode::kLocatorNode );
    MCHECKSTATUS(status, "registering sliderConstraintNode")
    MDGMessage::addNodeRemovedCallback(sliderConstraintNode::nodeRemoved, sliderConstraintNode::typeName);

	//
    status = plugin.registerNode( sixdofConstraintNode::typeName, sixdofConstraintNode::typeId,
                                  sixdofConstraintNode::creator,
                                  sixdofConstraintNode::initialize,
                                  MPxNode::kLocatorNode );
    MCHECKSTATUS(status, "registering sixdofConstraintNode")
    MDGMessage::addNodeRemovedCallback(sixdofConstraintNode::nodeRemoved, sixdofConstraintNode::typeName);

	// 
    status = plugin.registerNode( dSolverNode::typeName, dSolverNode::typeId,
                                  dSolverNode::creator, 
                                  dSolverNode::initialize,
//                                  MPxNode::kDependNode );
                                  MPxNode::kLocatorNode );
    MCHECKSTATUS(status, "registering dSolverNode")

    status = plugin.registerCommand( dSolverCmd::typeName,
                                     dSolverCmd::creator,
                                     dSolverCmd::syntax);
    MCHECKSTATUS(status, "registering dSolverCmd")

    status = plugin.registerCommand( dRigidBodyCmd::typeName,
                                     dRigidBodyCmd::creator,
                                     dRigidBodyCmd::syntax);
    MCHECKSTATUS(status, "registering dRigidBodyCmd")


    status = plugin.registerCommand( dRigidBodyArrayCmd::typeName,
                                     dRigidBodyArrayCmd::creator,
                                     dRigidBodyArrayCmd::syntax);
    MCHECKSTATUS(status, "registering dRigidBodyArrayCmd")

    status = plugin.registerCommand( dNailConstraintCmd::typeName,
                                     dNailConstraintCmd::creator,
                                     dNailConstraintCmd::syntax);
    MCHECKSTATUS(status, "registering dNailConstraintCmd")

    status = plugin.registerCommand( dHingeConstraintCmd::typeName,
                                     dHingeConstraintCmd::creator,
                                     dHingeConstraintCmd::syntax);
    MCHECKSTATUS(status, "registering dHingeConstraintCmd")

	status = plugin.registerCommand( dSliderConstraintCmd::typeName,
                                     dSliderConstraintCmd::creator,
                                     dSliderConstraintCmd::syntax);
    MCHECKSTATUS(status, "registering dSliderConstraintCmd")

	status = plugin.registerCommand( dSixdofConstraintCmd::typeName,
                                     dSixdofConstraintCmd::creator,
                                     dSixdofConstraintCmd::syntax);
    MCHECKSTATUS(status, "registering dSixdofConstraintCmd")

	status = plugin.registerCommand( dSoftBodyCmd::typeName,
		dSoftBodyCmd::creator, dSoftBodyCmd::syntax);
	MCHECKSTATUS(status, "registering dSoftBodyCmd")

	status = plugin.registerNode( SoftBodyNode::typeName, SoftBodyNode::typeId,
                                  SoftBodyNode::creator,
                                  SoftBodyNode::initialize,
                                  MPxNode::kLocatorNode );
	MDGMessage::addNodeRemovedCallback(SoftBodyNode::nodeRemoved, SoftBodyNode::typeName);
    MCHECKSTATUS(status, "registering SoftBodyNode")

	MGlobal::executeCommand( "source dynamicaUI.mel" );
    MGlobal::executeCommand( "dynamicaUI_initialize" );
    

	fMayaExitingCB				= MSceneMessage::addCallback( MSceneMessage::kMayaExiting,				releaseCallback, 0);

    return status;
}



MStatus uninitializePlugin( MObject obj )
{
    MStatus   status;
    MFnPlugin plugin( obj );

    status = plugin.deregisterCommand(dNailConstraintCmd::typeName);
    MCHECKSTATUS(status, "deregistering dNailConstraintCmd")

    status = plugin.deregisterCommand(dHingeConstraintCmd::typeName);
    MCHECKSTATUS(status, "deregistering dHingeConstraintCmd")

    status = plugin.deregisterCommand(dSliderConstraintCmd::typeName);
    MCHECKSTATUS(status, "deregistering dSliderConstraintCmd")

    status = plugin.deregisterCommand(dSixdofConstraintCmd::typeName);
    MCHECKSTATUS(status, "deregistering dSixdofConstraintCmd")


	status = plugin.deregisterCommand(dRigidBodyArrayCmd::typeName);
    MCHECKSTATUS(status, "deregistering dRigidBodyArrayCmd")

    status = plugin.deregisterCommand(dRigidBodyCmd::typeName);
    MCHECKSTATUS(status, "deregistering dRigidBodyCmd")

    status = plugin.deregisterCommand(dSolverCmd::typeName);
    MCHECKSTATUS(status, "deregistering dSolverCmd")

	status = plugin.deregisterCommand(dSoftBodyCmd::typeName);
	MCHECKSTATUS(status, "deregistering dSoftBodyCmd")


    status = plugin.deregisterNode(nailConstraintNode::typeId);
    MCHECKSTATUS(status, "deregistering nailConstraintNode")

	status = plugin.deregisterNode(hingeConstraintNode::typeId);
    MCHECKSTATUS(status, "deregistering hingeConstraintNode")

	status = plugin.deregisterNode(sliderConstraintNode::typeId);
    MCHECKSTATUS(status, "deregistering sliderConstraintNode")

	status = plugin.deregisterNode(sixdofConstraintNode::typeId);
    MCHECKSTATUS(status, "deregistering sixdofConstraintNode")

	status = plugin.deregisterNode(collisionShapeNode::typeId);
    MCHECKSTATUS(status, "deregistering collisionShapeNode")

    status = plugin.deregisterNode(rigidBodyArrayNode::typeId);
    MCHECKSTATUS(status, "deregistering rigidBodyArrayNode")

    status = plugin.deregisterNode(rigidBodyNode::typeId);
    MCHECKSTATUS(status, "deregistering rigidBodyNode")

	status = plugin.deregisterNode(dSolverNode::typeId);
    MCHECKSTATUS(status, "deregistering dSolverNode")

	status = plugin.deregisterNode(SoftBodyNode::typeId);
	MCHECKSTATUS(status, "deregistering SoftBodyNode")

    status =  plugin.deregisterFileTranslator( "Bullet Physics export" );
    MCHECKSTATUS(status,"deregistering Bullet Physics export" )


#ifdef BT_USE_COLLADA
	status =  plugin.deregisterFileTranslator( "COLLADA Physics export" );
    MCHECKSTATUS(status,"deregistering COLLADA Physics export" )
#endif //BT_USE_COLLADA

	solver_t::destroyWorld();

    solver_t::cleanup();

	exitCL();

    return status;
}


